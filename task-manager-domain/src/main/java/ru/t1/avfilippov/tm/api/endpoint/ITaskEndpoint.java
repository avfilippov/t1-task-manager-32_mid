package ru.t1.avfilippov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.dto.request.*;
import ru.t1.avfilippov.tm.dto.response.*;

public interface ITaskEndpoint {

    @NotNull BindTaskToProjectResponse bindTaskToProject(@NotNull BindTaskToProjectRequest request);

    @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull TaskCompleteByIdResponse completeTaskById(@NotNull TaskCompleteByIdRequest request);

    @NotNull TaskCompleteByIndexResponse completeTaskByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull TaskListResponse listTasks(@NotNull TaskListRequest request);

    @NotNull TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request);

    @NotNull TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull TaskShowByProjectIdResponse showTaskByProjectId(@NotNull TaskShowByProjectIdRequest request);

    @NotNull TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request);

    @NotNull TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull UnbindTaskFromProjectResponse unbindTaskFromProject(@NotNull UnbindTaskFromProjectRequest request);

    @NotNull TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

}
