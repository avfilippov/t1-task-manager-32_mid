package ru.t1.avfilippov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.dto.request.*;
import ru.t1.avfilippov.tm.dto.response.*;

public interface IProjectEndpoint {

    @NotNull ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull ProjectCompleteByIdResponse completeProjectById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull ProjectCompleteByIndexResponse completeProjectByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull ProjectListResponse listProjects(@NotNull ProjectListRequest request);

    @NotNull ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull ProjectShowByIdResponse showProjectById(@NotNull ProjectShowByIdRequest request);

    @NotNull ProjectShowByIndexResponse showProjectByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request);

    @NotNull ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
