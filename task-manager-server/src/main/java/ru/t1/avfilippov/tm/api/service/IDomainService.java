package ru.t1.avfilippov.tm.api.service;

public interface IDomainService {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void saveDataBinary();

    void loadDataBinary();

    void saveDataJsonFasterXml();

    void loadDataJsonFasterXml();

    void saveDataJsonJaxB();

    void loadDataJsonJaxB();

    void saveDataXmlFasterXml();

    void loadDataXmlFasterXml();

    void saveDataXmlJaxB();

    void loadDataXmlJaxB();

    void saveDataYaml();

    void loadDataYaml();

}
