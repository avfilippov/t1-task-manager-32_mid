package ru.t1.avfilippov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

}
