package ru.t1.avfilippov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.endpoint.*;
import ru.t1.avfilippov.tm.api.repository.IProjectRepository;
import ru.t1.avfilippov.tm.api.repository.ITaskRepository;
import ru.t1.avfilippov.tm.api.repository.IUserRepository;
import ru.t1.avfilippov.tm.api.service.*;
import ru.t1.avfilippov.tm.dto.request.*;
import ru.t1.avfilippov.tm.endpoint.*;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.model.User;
import ru.t1.avfilippov.tm.repository.ProjectRepository;
import ru.t1.avfilippov.tm.repository.TaskRepository;
import ru.t1.avfilippov.tm.repository.UserRepository;
import ru.t1.avfilippov.tm.service.*;
import ru.t1.avfilippov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);




    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::loadDataJsonJaxB);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::saveDataJsonJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadDataXmlJaxB);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveDataXmlJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadDataYaml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::saveDataYaml);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskListRequest.class, taskEndpoint::listTasks);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startTaskById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startTaskByIndex);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeTaskById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeTaskByIndex);
        server.registry(BindTaskToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(UnbindTaskFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::showTaskByProjectId);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectListRequest.class, projectEndpoint::listProjects);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startProjectById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startProjectByIndex);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeProjectById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);
        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);

    }


    public void start() {
        initPID();
        initDemoData();
        loggerService.info("*** TASK MANAGER SERVER STARTED ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        backup.init();
        server.start();
    }

    private void stop() {
        loggerService.info("*** TASK MANAGER SERVER IS SHUTTING DOWN***");
        backup.stop();
        try {
            server.stop();
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("TEST", "TEST", "test@test.ru");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST1", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("TEST2", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("TEST3", Status.IN_PROGRESS));

        taskService.add(test.getId(), new Task("TEST1", Status.NOT_STARTED));
        taskService.add(admin.getId(), new Task("TEST2", Status.NOT_STARTED));
    }

}
