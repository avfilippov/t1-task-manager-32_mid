package ru.t1.avfilippov.tm.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    @Nullable
    protected String userId = null;

    public AbstractServerSocketTask(
            @NotNull Server server,
            @NotNull Socket socket
    ) {
        super(server);
        this.socket = socket;
    }

    public AbstractServerSocketTask(
            @NotNull Server server,
            @NotNull Socket socket,
            @Nullable String userId
    ) {
        super(server);
        this.socket = socket;
        this.userId = userId;
    }

}
